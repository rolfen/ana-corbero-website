<!DOCTYPE html>
<html>
<head>
	<style type="text/css">
	.comment {
		color:green;
	}
	</style>
	<title></title>
</head>
<body>
	<h1>Ana Corbero Website Sitemap</h1>
	<ol class="content">
		<li>
			<h2>About</h2>		
			<ul>
				<li>Introduction <span class="comment">(what exactly is is? Is it "Biography" from the current website?)</span></li>
				<li>Portrait <span class="comment">(is it ok to just have the photo of the eye?)</span></li>
			</ul>
			<p>Refer to i & i = us book for:</p>
			<ul>
				<li>Photo of the eye (p.97)</li>
				<li>Chronological CV (p.96-98)</li>
			</ul>
		</li>
		<li>
			<h2>Painting and Drawings and Textiles</h2>
			<ol>
				<li>Paintings</li>
				<li>Brush Drawings on Canvas, Elegia Etrusca</li>
				<li>India Ink on Japanese Paper</li>
				<li>Woodcuts on Japanese Paper</li>
				<li>Rice Paper & India Ink, Erotikinks</li>
				<li>Postcards, Posters & Collage</li>
				<li>Textiles <span class="comment">(I & I printed towels)</span></li>
			</ol>
		</li>
		<li>
			<h2>Sculpture and Ceramics</h2>		
		</li>
		<li>
			<h2>Poetry, Video and Performance</h2>		
		</li>
		<li>
			<h2><a target="_blank" href="http://maus-haus.com">Design</a></h2>		
		</li>
		<li>
			<h2>Other</h2>
			<p>(Activism)</p>
			<ul>
				<li>i and i equal us (web link to main i&i website)</li>
				<li>Peace in our Lifetimes <span class="comment">(What is this?)</span></li>
				<li><a href="https://www.facebook.com/alandalusconnect">Andalus connect</a></li>
			</ul>
		</li>
	</ol>
</body>
</html>