/****************
/ 
/  slideshow class. Set up a parent container (with overflow hidden) and child containers with "page" class
/  page will also have class "prev", "next" or "current" set depending on their position (i think you need to initialize them)
/  you can also set a navRoot in options. The navRoot element contains items with class "items".
/  If you're on the 3rd page, the 3rd item inside navRoot will have it's class set to current. All others will have "current" unset
/
/  Basic usage: mySlideshow = new slideshow(rootElem);
/
/  Requires polyfills for IE8, because it uses JavaScript features from the latest specs
/  Requires nwmatcher.js
/
/  Options:
/  - navRoots (can take an array if multiple navRoots)
/  - onNav(this): callback, called on navigation and on slideshow init
/
/  loop setting values:
/	  0 or false (or some garbage): don't loop (default behavior)
/     1 or true: loop to the right only
/	  2: loop to the left only
/     3: loop in both directions
/
*/


function slideshow(root, options) {
	'use strict';

	/*
	this.classes = {

	};
	*/

    var slideshowInstance = this;

	if(typeof options == 'undefined') {
		this.options = {};
	} else {
		this.options = options;
		if(this.options && this.options.navRoot ) {
			if(this.options.navRoot instanceof Array) {
				this.options.navRoots = this.options.navRoot;
			} else {
				this.options.navRoots = [this.options.navRoot];
			}
		}
	}
    this.timer = null;

    this.isLast = function() {
		return((slideshowInstance.getPages().length -2) < slideshowInstance.getCurrentPageIndex());
	}

    this.isFirst = function() {
    	return(slideshowInstance.getCurrentPageIndex() < 1);
    }

    this.getPages = function() {
		return(NW.Dom.select( ".page", root));
    }

    this.getCurrentPageIndex = function() {
		var pages = slideshowInstance.getPages();
		var currPage = NW.Dom.select( ".page.current", root)[0];
		var i = pages.indexOf(currPage);
		return(i);
    }

    this.getPageIndex = function(i,loop) {
    	// if index is not valid, return closest valid index depending on the loop setting
    	var target;
		var pages = slideshowInstance.getPages();
		
		// make sure we don't go beyond last / before first
		if(pages[i]) {
			target = i;
		} else {
            // we are at or beyond boundaries
            var isBeyondEnd = i >= pages.length;
            var isBeforeStart = i < 0;
            if(pages.length == 0) {
            	target = 0;
            } else if(loop == true || loop === 1) {
            	target = 0;
            } else if(loop === 2) {
				target = pages.length - 1;
            } else if(loop === 3) {
            	if(isBeyondEnd) {
            		target = 0;
            	} else if(isBeforeStart) {
            		target = pages.length - 1;
            	} 
            } else {
            	// loop defaults to false
            	if(isBeyondEnd) {
            		target = pages.length - 1;
            	} else if(isBeforeStart) {
            		target = 0;
            	}
            } 
		}

		return(target);
    }

    this.startAuto = function(interval){
        if(interval === undefined || interval === null) interval = 3;
        slideshowInstance.stopAuto();
        this.timer = setInterval(function(){
            slideshowInstance.nextPageIfAvailable(true);
        }, interval * 1000);
    }
    
    this.stopAuto = function() {
       if(this.timer !== undefined || this.timer !== null) {
           clearInterval(this.timer);
           this.timer = null;
       }
    }
    this.pageIsAvailable = function(i, loop) {
    	// page content is loaded? ( = iql-status is set to loaded)
        if(loop === undefined || loop === null) loop = false;
    	var realPage = slideshowInstance.getPage(i, loop);
    	if(
			(
				realPage.hasAttribute('iql-status') 
				&& (realPage.getAttribute('iql-status')=="loaded")
			) || realPage.hasAttribute('src') 
		) {
    		return(true)
    	} else {
    		return(false)
    	}
    }
    this.nextPageIfAvailable = function(loop) {
    	// goes to next page if next page content is loaded ( = iql-status is set to loaded)
        if(loop === undefined || loop === null) loop = false;
    	if(slideshowInstance.pageIsAvailable(slideshowInstance.getCurrentPageIndex() +1, loop)) {
    		slideshowInstance.nextPage(loop);
    	}
    }
	this.nextPage = function(loop) {
        if(loop === undefined || loop === null) loop = false;
		this.page( slideshowInstance.getCurrentPageIndex() +1, loop);
	}
	this.prevPage = function(loop) {
        if(loop === undefined || loop === null) loop = false;
		this.page( slideshowInstance.getCurrentPageIndex() -1, loop);
	}

	this.firstPage = function() {
		this.page(0);
	}

	this.lastPage = function(){
		this.page(pages.length - 1);
	}

    this.getPage = function(i, loop) {
        if(loop === undefined || loop === null) loop = false;
		var pages = slideshowInstance.getPages();
		var target = slideshowInstance.getPageIndex(i, loop);
		return(pages[target]);
    }

	this.page = function(i, loop) {
        if(loop === undefined || loop === null) loop = false;

		var pages = slideshowInstance.getPages();
		var target = slideshowInstance.getPageIndex(i, loop);

		pages.forEach(function(elem){
			// clear all styles
			// removeClass(elem, "current");
			// removeClass(elem, "prev");
			// removeClass(elem, "next");
			// set prev for all elements before and next for all after
			var elemIndex = pages.indexOf(elem);
			if(elemIndex < target) {
				removeClass(elem, "current");
				removeClass(elem, "next");
				addClass(elem, "prev");
			} else if(elemIndex == target) {
				removeClass(elem, "prev");
				removeClass(elem, "next");
				addClass(elem, "current");				
			} else if(elemIndex > target) {
				removeClass(elem, "current");
				removeClass(elem, "prev");
				addClass(elem, "next");
			}
		});


		// set current, prev and next classes - the old way, this would only set the elements directly after and before
		/*
		addClass(pages[target],"current");
		if(pages[target-1]) {
			addClass(pages[target-1],"prev");
		}
		if(pages[target+1]) {
			addClass(pages[target+1],"next");
		}
		*/

		// set navigation
		if (this.options && this.options.navRoots) {
			slideshowInstance.options.navRoots.forEach(function(navRoot){
				var navSelected = NW.Dom.select(
					".item",
					navRoot,
					function(elem) {
						removeClass(elem, "selected");
					}
				)[target];
				navSelected && addClass(navSelected, "selected");
			});
		}

		// nav callback
		if (this.options && this.options.onNav) {
			this.options.onNav(slideshowInstance);
		}
	}

	// init callback
	if (this.options && this.options.onNav) {
		this.options.onNav(slideshowInstance);
	}
}

function calculateSmartRangeBoundaries(itemCount, selectedItem, contextSize) {
	// smart navigation/pagination menu helper function
	// returns the range as array [lowerBoundary,upperBoundary]
	// with selectedItem as close as possible to the center of the range
	var maxSize = ( contextSize * 2 ) + 1
	var upperBoundary, lowerBoundary
	if ( itemCount > maxSize ) {
		upperBoundary = selectedItem + contextSize
		lowerBoundary = selectedItem - contextSize
		if ( lowerBoundary < 0 ) {
			upperBoundary -= lowerBoundary
			lowerBoundary = 0
		} else if ( upperBoundary  > itemCount ) {
			lowerBoundary -= ( upperBoundary - itemCount )
			upperBoundary = itemCount
		}
	} else {
		// display as is (no upper and lower boundaries)
		lowerBoundary = 0
		upperBoundary = itemCount
	}
	return([lowerBoundary, upperBoundary])
}