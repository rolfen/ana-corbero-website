<?

$HomeURI = "./";

?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

	<link rel="stylesheet" type="text/css" href="css/jquery.fancybox.css" media="screen" />
	<script type="text/javascript" src="js/nwmatcher.min.js"></script>
	<script type="text/javascript" src="js/slideshow.js"></script>
	<script type="text/javascript">

	</script>

	<title></title>
	<link type="text/css" rel="stylesheet" media="" href="css/user.css" />
</head>
<body class="<?= isset($_GET['lightbox']) ? "no-scroll" : "" ?>">
	<div class="header">
		<div class="menu">
			<div class="item">
				<img class="title" src="img/title.gif">
			</div>
			<div class="item">Sculpture and Ceramics</div>
			<div class="item selected">Painting, Drawings &amp; Textiles</div>
			<div class="item">Poetry, Video &amp; Performance</div>
			<div class="item">Design</div>
			<div class="item">Activism, Other</div>
			<div class="item">About</div>
		</div>
	</div>
	<div class="lightbox <?= isset($_GET['lightbox']) ? "lightbox-on" : "" ?>">
		<div class="page prev">
			<img class="image" src="img/1.jpg">
			<div class="legend">
				Through the Vine - 1988
			</div>
			<a href="<?= $HomeURI ?>" class="clickable-back"></a>
		</div>
		<div class="page current">
			<img class="image" src="img/4.jpg">
			<div class="legend">
				Bon Voyage - 1990
			</div>
			<a href="<?= $HomeURI ?>" class="clickable-back"></a>
		</div>
		<div class="page next">
			<img class="image" src="img/5.jpg">
			<div class="legend">
				Pray for Me - 1987							
			</div>
			<a href="<?= $HomeURI ?>" class="clickable-back"></a>
		</div>	
		<div class="page next">
			<img class="image" src="img/6.jpg">
			<div class="legend">
				Pray for Me - 1987							
			</div>
			<a href="<?= $HomeURI ?>" class="clickable-back"></a>
		</div>	
		<div class="page next">
			<img class="image" src="img/7.jpg">
			<div class="legend">
				Pray for Me - 1987							
			</div>
			<a href="<?= $HomeURI ?>" class="clickable-back"></a>
		</div>
		<div class="nav">
			<div class="arrow left"></div>
			<div class="arrow right"></div>
		</div>	
	</div>
	<div class="page">
		<h1>Painting, Drawings &amp; Textiles</h1>
		<h2>Postcards, Posters &amp; Collages</h2>
		<div class="section">
			<div class="strip">
				<div class="thumbs">
					<a href="?lightbox" class="item">
						<img class="thumb" src="img/1.jpg">
						<div class="legend">
							<div class="title">Through the Vine</div>
							1988
						</div>
					</a>
					<div class="item">
						<img class="thumb" src="img/4.jpg">
						<div class="legend">
							<div class="title">Bon Voyage</div>	
							1990
						</div>
					</div>
					<div class="item">
						<img class="thumb" src="img/5.jpg">
						<div class="legend">
							<div class="title">Pray for Me</div>	
							1987							
						</div>
					</div>
					<div class="item">
						<img class="thumb" src="img/6.jpg">
						<div class="legend">
							<div class="title">I pray...</div>	
							1998							
						</div>
					</div>
					<div class="item">
						<img class="thumb" src="img/7.jpg">	
						<div class="legend">
							<div class="title">Let me tell you</div>	
							1998							
						</div>				
					</div>
				</div>
				<div class="nav">
					<div class="right arrow"></div>
				</div>
			</div>
		</div>
		<h2>Paintings</h2>
		<div class="section">
			<div class="strip">
				<div class="thumbs">
					<div class="item">
						<img class="thumb" src="img/p1.jpg">
						<div class="legend">
							<div class="title">Coral Cumulus</div>	
							2013						
						</div>
					</div>
					<div class="item">
						<img class="thumb" src="img/p2.jpg">
						<div class="legend">
							<div class="title">Jet</div>	
							2013							
						</div>
					</div>
					<div class="item">
						<img class="thumb" src="img/p3.jpg">
						<div class="legend">
							<div class="title">Pink Numbus</div>	
							2013	
						</div>
					</div>
					<div class="item">
						<img class="thumb" src="img/p4.jpg">
						<div class="legend">
							<div class="title">Puce Flush</div>	
							2013							
						</div>
					</div>
					<div class="item">
						<img class="thumb" src="img/p5.jpg">
						<div class="legend">
							<div class="title">Rainy</div>	
							2013							
						</div>				
					</div>
					<div class="item">
						<img class="thumb" src="img/p6.jpg">
						<div class="legend">
							<div class="title">Sky of the sea of Marmara</div>	
							2013							
						</div>
					</div>
					<div class="item">
						<img class="thumb" src="img/p7.jpg">
						<div class="legend">
							<div class="title">Tangerine Dreams</div>	
							2013							
						</div>				
					</div>
				</div>
				<div class="nav">
					<div class="right arrow"></div>
				</div>
			</div>
		</div>
		<h2>Postcards, Posters &amp; Collages</h2>
		<div class="section">
			<div class="strip">
				<div class="thumbs">
					<div class="item">
						<img class="thumb" src="img/1.jpg">
						<div class="legend">
							<div class="title">Through the Vine</div>
							1988
						</div>
					</div>
					<div class="item">
						<img class="thumb" src="img/4.jpg">
						<div class="legend">
							<div class="title">Bon Voyage</div>	
							1990
						</div>
					</div>
					<div class="item">
						<img class="thumb" src="img/5.jpg">
						<div class="legend">
							<div class="title">Pray for Me</div>	
							1987							
						</div>
					</div>
					<div class="item">
						<img class="thumb" src="img/6.jpg">
						<div class="legend">
							<div class="title">I pray...</div>	
							1998							
						</div>
					</div>
					<div class="item">
						<img class="thumb" src="img/7.jpg">	
						<div class="legend">
							<div class="title">Let me tell you</div>	
							1998							
						</div>				
					</div>
				</div>
				<div class="nav">
					<div class="right arrow"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>